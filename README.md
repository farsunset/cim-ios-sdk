# CIMSDK_iOS

#### 介绍
CIMSDK_iOS是基于[CIM项目](https://gitee.com/farsunset/cim)框架下的iOS端SDK。配合CIM服务我们可以快速搭建属于自己的推送系统、IM系统。


Requirements
----------------
项目要求 :
+ ```iOS10```

Podfile
----------------
```
use_frameworks!

pod 'CIMSDK_iOS'

```

然后运行一下命令安装:

```
$ pod install
```

使用方法
----------------
#####  1. 导入头文件
```
#import "CIMService.h"

```
#####  2. 初始化配置
```
[[CIMService sharedInstance] configHost:@"120.53.221.43" onPort:23456];
```
#####  3. 添加监听回调
```
[[CIMService sharedInstance] addServiceObserver:self];

```
#####  4. 连接服务
```
[[CIMService sharedInstance] connection];
```
#####  5. 在连接成功的回调中绑定用户
```
- (void)serviceConnectSuccess {
    [[CIMService sharedInstance] bindUserId:@"2222"];
}
```
#####  6. CIMServiceObserver回调说明
```
/**
 *  将要开始连接
 **/
- (void)serviceWillConnect:(CIMService *)service;

/**
 *  开始连接时出错
 **/
- (void)serviceWillConnect:(CIMService *)service error:(NSError *)error;

/**
 *  连接成功
 **/
- (void)serviceConnectSuccess:(CIMService *)service;

/**
   重连回调
 */
- (void)service:(CIMService *)service didReconnection:(NSInteger)reconnectionCount;

/**
 *  连接断开
 **/
- (void)serviceDidDisconnect:(CIMService *)service error:(NSError *)error;

/**
   接收到消息
 */
- (void)service:(CIMService *)service receiveMessage:(CIMMessage *)message;

/**
   接收到ReplyBody
 */
- (void)service:(CIMService *)service receiveReplyBody:(CIMReplyBody *)replyBody;

/**
   数据无法解析
 */
- (void)service:(CIMService *)service unableParseData:(NSData *)data;

```
#####  7. 移除监听
```
[[CIMService sharedInstance] removeServiceObserver:self];
```
#####  8. 向服务端发送消息
```
CIMSentBody *sentBody;
//设置参数 ......
[[CIMService sharedInstance] sendRequest:sentBody];
```
#####  9. 其他方法说明
```
+ (CIMService *)sharedInstance;

/**
   返回当前连接的状态；
 */
@property (nonatomic, readonly) BOOL isConnected;

/**
   指示由于网络原因导致连接断开是否自动重连,reconnection = YES 自动重连，reconnection = NO 不自动重连；
   默认reconnection = YES；
   重连逻辑受reconnectionTimeInterval和reconnectionMaxCount影响；
 */
@property (nonatomic, assign) BOOL reconnection;

/**
   当reconnection = YES时自动重连的时间间隔，默认reconnectionTimeInterval=1.0 单位：秒；
 */
@property (nonatomic, assign) NSTimeInterval reconnectionTimeInterval;

/**
   当reconnection = YES时自动重连的最大次数，默认reconnectionMaxCount=3；
 */
@property (nonatomic, assign) NSInteger reconnectionMaxCount;

/**
   配置IM服务器
 */
- (void)configHost:(NSString *)host onPort:(NSInteger)port;

/**
   连接到host:port对应的服务器
 */
- (void)connection;

/**
   绑定userId,需要根据isConnected判断当前连接的状态；
   如果isConnected=NO需要先调用[[CIMService instance] connection],等到连接成功后再调用[[CIMService instance] bindUserId:];
   如果isConnected=YES,可直接调用[[CIMService instance] bindUserId:];
 */
- (void)bindUserId:(NSString *)userId;

/**
   发送请求到服务
 */
- (void)sendRequest:(CIMSentBody *)sendBody;

/**
   添加消息监听回调,(可添加多个)不使用时记得Remove
 */
- (void)addServiceObserver:(id<CIMServiceObserver>)observer;

/**
   移除监听
 */
- (void)removeServiceObserver:(id<CIMServiceObserver>)observer;

/**
   断开连接
 */
- (void)disconnect;

```

帮助和建议
----------------
如果你有好的建议和帮助您可以直接联系我：chen_tao@me.com




