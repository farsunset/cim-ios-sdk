#
# Be sure to run `pod lib lint CIMSDK_iOS.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CIMSDK_iOS'
  s.version          = '0.0.4'
  s.summary          = 'CIMSDK是基于CIM项目框架下的iOS端SDK。'
  s.description      = 'CIMSDK是基于CIM项目框架下的iOS端SDK。配合CIM服务我们可以快速搭建属于自己的推送系统、IM系统。'

  s.homepage         = 'https://gitee.com/farsunset/cim-ios-sdk'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { "Chentao" => "chen_tao@me.com" }
  s.source       = { :git => "https://gitee.com/farsunset/cim-ios-sdk.git", :tag => s.version }

  s.ios.deployment_target = '10.0'

  s.source_files = 'CIMSDK/Classes/**/*'

  s.dependency 'Protobuf', '~> 3.21.5'
  s.dependency 'CocoaAsyncSocket', '~> 7.6.5'

end
