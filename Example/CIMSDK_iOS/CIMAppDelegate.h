//
//  CIMAppDelegate.h
//  CIMSDK_iOS
//
//  Created by Chentao on 09/17/2022.
//  Copyright (c) 2022 Chentao. All rights reserved.
//

@import UIKit;

@interface CIMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
