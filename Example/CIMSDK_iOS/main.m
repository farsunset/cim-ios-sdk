//
//  main.m
//  CIMSDK_iOS
//
//  Created by Chentao on 09/17/2022.
//  Copyright (c) 2022 Chentao. All rights reserved.
//

@import UIKit;
#import "CIMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CIMAppDelegate class]));
    }
}
