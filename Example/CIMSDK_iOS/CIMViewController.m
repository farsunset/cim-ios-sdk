//
//  CIMViewController.m
//  CIMSDK
//
//  Created by Chentao on 09/17/2022.
//  Copyright (c) 2022 Chentao. All rights reserved.
//

#import "CIMViewController.h"
#import "CIMService.h"

@interface CIMViewController ()<CIMServiceObserver>

@property (nonatomic, strong) UIButton *addObserverButton;

@property (nonatomic, strong) UIButton *removeObserverButton;

@property (nonatomic, strong) UIButton *connectButton;

@property (nonatomic, strong) UIButton *sendButton;

@property (nonatomic, strong) UIButton *closeButton;


@end

@implementation CIMViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[CIMService sharedInstance] configHost:@"120.53.221.43" onPort:23456];

    self.addObserverButton = [[UIButton alloc] initWithFrame:CGRectMake(50, 100, 150, 50)];
    self.addObserverButton.backgroundColor = [UIColor grayColor];
    [self.addObserverButton addTarget:self action:@selector(addObserverButtonHandler) forControlEvents:UIControlEventTouchUpInside];
    [self.addObserverButton setTitle:@"addObserver" forState:UIControlStateNormal];
    [self.view addSubview:self.addObserverButton];

    self.removeObserverButton = [[UIButton alloc] initWithFrame:CGRectMake(220, 100, 150, 50)];
    self.removeObserverButton.backgroundColor = [UIColor grayColor];
    [self.removeObserverButton addTarget:self action:@selector(removeObserverButtonHandler) forControlEvents:UIControlEventTouchUpInside];
    [self.removeObserverButton setTitle:@"removeObserver" forState:UIControlStateNormal];
    [self.view addSubview:self.removeObserverButton];

    self.connectButton = [[UIButton alloc] initWithFrame:CGRectMake(50, 170, 150, 50)];
    self.connectButton.backgroundColor = [UIColor grayColor];
    [self.connectButton addTarget:self action:@selector(connectButtonHandler) forControlEvents:UIControlEventTouchUpInside];
    [self.connectButton setTitle:@"connec" forState:UIControlStateNormal];
    [self.view addSubview:self.connectButton];

    self.sendButton = [[UIButton alloc] initWithFrame:CGRectMake(50, 240, 150, 50)];
    self.sendButton.backgroundColor = [UIColor grayColor];
    [self.sendButton addTarget:self action:@selector(sendButtonHandler) forControlEvents:UIControlEventTouchUpInside];
    [self.sendButton setTitle:@"send" forState:UIControlStateNormal];
    [self.view addSubview:self.sendButton];

    self.closeButton = [[UIButton alloc] initWithFrame:CGRectMake(220, 170, 150, 50)];
    self.closeButton.backgroundColor = [UIColor grayColor];
    [self.closeButton addTarget:self action:@selector(closeButtonHandler) forControlEvents:UIControlEventTouchUpInside];
    [self.closeButton setTitle:@"close" forState:UIControlStateNormal];
    [self.view addSubview:self.closeButton];
}

- (void)addObserverButtonHandler {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[CIMService sharedInstance] addServiceObserver:self];
    });
}

- (void)removeObserverButtonHandler {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[CIMService sharedInstance] removeServiceObserver:self];
    });
}

- (void)connectButtonHandler {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[CIMService sharedInstance] connection];
    });
}

- (void)sendButtonHandler {
//    //向服务端发送消息
//    CIMSentBody *clientBindBody = [[CIMSentBody alloc] init];
//
//    clientBindBody.key = @"client_bind";
//    clientBindBody.timestamp = (int64_t)[NSDate timeIntervalSinceReferenceDate] * 1000;
//
//    NSMutableDictionary<NSString *, NSString *> *data = [[NSMutableDictionary alloc] init];
//    [data setValue:@"123456" forKey:@"uid"];
//    [data setValue:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"deviceId"];
//    [data setValue:@"ios" forKey:@"channel"];
//    [data setValue:[[UIDevice currentDevice] name] forKey:@"deviceName"];
//
//
//    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
//    [data setValue:[infoDictionary objectForKey:@"CFBundleShortVersionString"] forKey:@"appVersion"];
//
//    [data setValue:[[UIDevice currentDevice] systemVersion] forKey:@"osVersion"];
//
//    NSArray<NSString *> *languages = [NSLocale preferredLanguages];
//    [data setValue:languages.firstObject forKey:@"language"];
//
//    clientBindBody.data = data;
//
//    [[CIMService sharedInstance] sendRequest:clientBindBody];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[CIMService sharedInstance] bindUserId:@"2222"];
    });
}

- (void)closeButtonHandler {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[CIMService sharedInstance] disconnect];
    });
}

#pragma mark - CIMServiceObserver

- (void)serviceWillConnect:(CIMService *)service {
    NSLog(@"CIMServiceObserver %s %@", __FUNCTION__ ,NSThread.currentThread);
}

- (void)serviceWillConnect:(CIMService *)service error:(NSError *)error {
    NSLog(@"CIMServiceObserver %s %@", __FUNCTION__ ,NSThread.currentThread);
}

- (void)serviceConnectSuccess:(CIMService *)service {
    NSLog(@"CIMServiceObserver %s %@", __FUNCTION__ ,NSThread.currentThread);
}

- (void)service:(CIMService *)service didReconnection:(NSInteger)reconnectionCount {
    NSLog(@"%s reconnectionCount:%@ %@", __FUNCTION__, @(reconnectionCount),NSThread.currentThread);
}

- (void)serviceDidDisconnect:(CIMService *)service error:(NSError *)error {
    NSLog(@"CIMServiceObserver %s %@", __FUNCTION__ ,NSThread.currentThread);
}

- (void)service:(CIMService *)service receiveMessage:(CIMMessage *)message {
    NSLog(@"CIMServiceObserver %s %@", __FUNCTION__ ,NSThread.currentThread);
    NSLog(@"content:%@", message.content);
}

- (void)service:(CIMService *)service receiveReplyBody:(CIMReplyBody *)replyBody {
    NSLog(@"CIMServiceObserver %s %@", __FUNCTION__ ,NSThread.currentThread);
}

- (void)service:(CIMService *)service unableParseData:(NSData *)data {
    NSLog(@"CIMServiceObserver %s %@", __FUNCTION__ ,NSThread.currentThread);
}

@end
