//
//  CIMMessage.h
//  CIMSDK
//
//  Created by Chentao on 2022/8/31.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CIMMessage : NSObject

@property (nonatomic, readwrite) int64_t id_p;

@property (nonatomic, readwrite, copy) NSString *action;

@property (nonatomic, readwrite, copy) NSString *content;

@property (nonatomic, readwrite, copy) NSString *sender;

@property (nonatomic, readwrite, copy) NSString *receiver;

@property (nonatomic, readwrite, copy) NSString *extra;

@property (nonatomic, readwrite, copy) NSString *title;

@property (nonatomic, readwrite, copy) NSString *format;

@property (nonatomic, readwrite) int64_t timestamp;

@end

NS_ASSUME_NONNULL_END
