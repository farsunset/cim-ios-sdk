//
//  CIMWeakProxy.h
//  CIMSDK
//
//  Created by Chentao on 2022/9/14.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CIMWeakProxy : NSProxy

@property (weak, readonly, nullable, nonatomic) id target;

- (nonnull instancetype)initWithTarget:(nonnull id)target;
+ (nonnull instancetype)proxyWithTarget:(nonnull id)target;

@end

NS_ASSUME_NONNULL_END
