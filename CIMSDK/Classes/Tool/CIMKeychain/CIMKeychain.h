//
//  CIMKeychain.h
//  CIMSDK_iOS
//
//  Created by Chentao on 2022/9/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CIMKeychain : NSObject

/**
 * 存储一个值到钥匙串
 *
 * @param value 要存储的值
 * @param key 存取的key
 **/
+ (void)saveValue:(id)value key:(NSString *)key;

/**
 * 从钥匙串获得一个值
 *
 * @param key 取值所需的key
 * @return 返回key对应的值
 **/
+ (id)getValueForKey:(NSString *)key;

/**
 * 从钥匙串删除一个值
 *
 * @param key 删除值所需的key
 **/
+ (void)deleteValueForKey:(NSString *)key;

/**
 * 从钥匙串中清空本应用的存储数据
 *
 **/
+ (void)clearAllConfig;

@end

NS_ASSUME_NONNULL_END
