//
//  CIMSentBody.h
//  CIMSDK_iOS
//
//  Created by Chentao on 2022/9/17.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CIMSentBody : NSObject

@property (nonatomic, copy) NSString *key;

@property (nonatomic, assign) int64_t timestamp;

@property (nonatomic, strong) NSDictionary<NSString *, NSString *> *data;

@end

NS_ASSUME_NONNULL_END
