//
//  CIMSignalServiceBuffer.h
//  CIMSDK
//
//  Created by Chentao on 2022/9/3.
//

#import <Foundation/Foundation.h>
#import "CIMSignalPacket.h"
NS_ASSUME_NONNULL_BEGIN

@class CIMSignalServiceBuffer;
@protocol CIMSignalServiceBufferDelegate <NSObject>

- (void)signalServiceBuffer:(CIMSignalServiceBuffer *)signalServiceBuffer receiveSignalPacket:(CIMSignalPacket *)signalPacket;

@end

@interface CIMSignalServiceBuffer : NSObject

@property (nonatomic, weak) id <CIMSignalServiceBufferDelegate> delegate;

- (void)appendData:(NSData *)data;

- (void)clear;

@end

NS_ASSUME_NONNULL_END
