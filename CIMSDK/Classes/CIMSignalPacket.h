//
//  CIMSignalPacket.h
//  CIMSDK
//
//  Created by Chentao on 2022/9/3.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

extern UInt8 const CIMSIGNAL_PACKET_HEAD_SIZE;

typedef NS_ENUM(UInt8, CIMSignalPacketType) {
    CIMSignalPacketTypePong      = 0,    //PONG
    CIMSignalPacketTypePing      = 1,    //PING
    CIMSignalPacketTypeMessage   = 2,    //Message
    CIMSignalPacketTypeSentBody  = 3,    //SentBody
    CIMSignalPacketTypeReplyBody = 4,    //ReplyBody
};

@interface CIMSignalPacket : NSObject

@property (nonatomic, assign) CIMSignalPacketType tag;
@property (nonatomic, assign) UInt16 bodyLength;
@property (nonatomic, strong) NSData *packetBody;

@end

NS_ASSUME_NONNULL_END
