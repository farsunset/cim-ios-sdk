//
//  CIMSignalService.h
//  CIMSDK
//
//  Created by Chentao on 2022/9/2.
//

#import <Foundation/Foundation.h>
#import "CIMSignalPacket.h"
NS_ASSUME_NONNULL_BEGIN

@interface CIMSignalServiceConfig : NSObject

@property (nonatomic, copy) NSString *host;
@property (nonatomic, assign) NSInteger port;

@end

@class CIMSignalService;
@protocol CIMSignalServiceDelegate <NSObject>

/**
 *  将要开始连接
 **/
- (void)signalServiceWillConnect:(CIMSignalService *)signalService;

/**
 *  开始连接时出错
 **/
- (void)signalServiceWillConnect:(CIMSignalService *)signalService error:(NSError *)error;

/**
 *  连接成功
 **/
- (void)signalServiceConnectSuccess:(CIMSignalService *)signalService;

/**
 *  连接断开
 **/
- (void)signalServiceDidDisconnect:(CIMSignalService *)signalService error:(NSError *)error;

/**
 *  收到新包
 **/
- (void)signalService:(CIMSignalService *)signalService receivePacket:(CIMSignalPacket *)packet;

@end

@interface CIMSignalService : NSObject

@property (nonatomic, weak) id<CIMSignalServiceDelegate> delegate;

@property (nonatomic, strong) CIMSignalServiceConfig *serviceConfig;

@property (nonatomic, readonly) BOOL isConnected;

- (void)connect;

- (void)close;

- (void)sendSignalPacket:(CIMSignalPacket *)signalPacket;

@end

NS_ASSUME_NONNULL_END
